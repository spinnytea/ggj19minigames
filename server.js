const express = require('express');
const serveIndex = require('serve-index');
const address = require('address');

const host = process.env.HOST || '0.0.0.0';
const port = (process.env.PORT && parseInt(process.env.PORT)) || 3000;

const app = express();

app.use(express.static(__dirname));
app.use(serveIndex(__dirname, { icons: true, filter: function (filename, index, files, dir) {
  if (/package(-lock)?\.json/.test(filename)) return false;
  if (/node_modules/.test(filename)) return false;
  if (dir === (__dirname + '/') && /dist/.test(filename)) return false;
  if (dir === (__dirname + '/') && /server\.js/.test(filename)) return false;
  if (dir === (__dirname + '/') && /build\.sh/.test(filename)) return false;
  return true;
} }));

app.listen(port, host, function () {
  console.log('✨ Server running ✨');
  console.log('');
  console.log(`  Local   - http://${host}:${port}`);
  console.log(`  Network - http://${address.ip()}:${port}`);
  console.log('');
});