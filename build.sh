#!/usr/bin/env bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

copyAllFiles () {
  # $1 is source
  # $2 is destination
  for f in $(ls "${1}"); do
    cp -r "${1}/${f}" "${2}"
  done
}

buildProject () {
  # $1 is project name
  # $2 is relative path
  # $3 is press photo

  BUILD_DIR=$(mktemp -d)
  pushd $BUILD_DIR
  mkdir -p "${1}-source/press"
  copyAllFiles "${SCRIPT_DIR}/${2}" "${BUILD_DIR}/${1}-source"
  cp "${SCRIPT_DIR}/README.md" "${1}-source"
  cp "${SCRIPT_DIR}/dist/license.txt" "${1}-source"
  cp "${SCRIPT_DIR}/${3}" "${1}-source/press"
  rm "${SCRIPT_DIR}/dist/dist-source-${1}.zip"
  zip -r -9 "${SCRIPT_DIR}/dist/dist-source-${1}.zip" "${1}-source"
  popd

  BUILD_DIR=$(mktemp -d)
  pushd $BUILD_DIR
  mkdir -p "${1}/press"
  copyAllFiles "${SCRIPT_DIR}/${2}" "${BUILD_DIR}/${1}"
  rm -rf "${BUILD_DIR}/${1}/raw"
  cp "${SCRIPT_DIR}/README.md" "${1}"
  cp "${SCRIPT_DIR}/dist/license.txt" "${1}"
  cp "${SCRIPT_DIR}/${3}" "${1}/press"
  rm "${SCRIPT_DIR}/dist/dist-release-${1}.zip"
  zip -r -9 "${SCRIPT_DIR}/dist/dist-release-${1}.zip" "${1}"
  popd
}

buildProject sticky-finger "Sticky Finger" dist/press-sticky.png
buildProject comfy-pixels "Comfy Pixels" dist/press-comfy.png
buildProject pet-the-doggo "Pet the Doggo" dist/press-doggo.png