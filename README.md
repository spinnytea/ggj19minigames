GGJ19 Minigames
===

## Sticky Fingers
[Sticky Fingers on GGJ](https://globalgamejam.org/2019/games/sticky-finger)  
[Short URL: http://bombch.us/C5ev](http://bombch.us/C5ev)

Instructions
> _read_


## Comfy Pixels
[Comfy Pixels on GGJ](https://globalgamejam.org/2019/games/comfy-pixels)  
[Short URL: http://bombch.us/C5eu](http://bombch.us/C5eu)

Instructions
> _click_  -OR- _press any key_  
> _enjoy_


## Pet the Doggo
[Pet the Doggo on GGJ](https://globalgamejam.org/2019/games/pet-doggo)  
[Short URL: http://bombch.us/C5et](http://bombch.us/C5et)

Instructions
> _click_


References
===

[GGJ 19 Diversifiers](https://globalgamejam.org/news/ggj19-diversifiers)
