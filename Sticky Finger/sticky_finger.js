(function() {
	const $body = $('body');
	const $text = $('.text');
	const baseURL = window.location.href.substring(0, window.location.href.lastIndexOf('/'));
	const scratch = new Audio(baseURL + '/audio/scratch.mp3');
	const racecar = new Audio(baseURL + '/audio/racecar-short.mp3');
	const randomScratchDelay = [10, 15, 30, 45, 60, 90, 120];
	const audioTracks = [
		'/audio/track1.mp3',
	];

	let scratching = false;
	let intervalId;
	let interruptId;
	let start = 0;
	let bgmusic;

	$(window).on('keydown touchstart', startGame);
	$(window).on('keyup touchend blur', stopGame);

	scratch.onended = function playRacecar () {
		racecar.play();
	}
	racecar.onended = function resumeAudio () {
		randomInterruption();
		scratching = false;
		$body.removeClass('scratching');
		bgmusic.play();
	}

	function startGame () {
		if(start) return;
		$text.text(0);
		start = Date.now();
		nextTrack(0);
		
		intervalId = setInterval(function updateTimerText () {
			$text.text(moment().hour(0).minute(0).second(0)
				.milliseconds(Date.now() - start).format('HH : mm : ss : SSS'));
		}, 19); // any slower and you start to notice the :00x millies
	}

	function stopGame () {
		clearInterval(intervalId);
		clearTimeout(interruptId);
		start = 0;
		scratching = false;
		$body.removeClass('scratching');
		stopAudio(bgmusic, scratch, racecar);
		$text.text('Press any key...');
	}

	function stopAudio (...audios) {
		audios.forEach(function (audio) {
			if (!audio) return;
			audio.pause();
			audio.currentTime = 0;
		});
	}

	function randomInterruption () {
		const scratchTime = randomScratchDelay[Math.floor(Math.random() * randomScratchDelay.length)];
		console.log('Next scratch in:', scratchTime);
		interruptId = setTimeout(function startScratch () {
			scratching = true;
			$body.addClass('scratching');
			bgmusic.pause();
			scratch.play();
		}, scratchTime * 1000);
	}

	function nextTrack (index) {
		if (bgmusic) stopAudio(bgmusic);
		bgmusic = new Audio(baseURL + audioTracks[index]);
		bgmusic.onended = function queueNextTrack () {
			nextTrack((index + 1) % audioTracks.length);
		};
		if (!scratching) {
			bgmusic.play();
			randomInterruption();
		}
	}

	stopGame();
})();